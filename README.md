# Scientific Data Management
## Bio-IT Course
### Contributors
- Holger Dinkel
- Charles Girardot
- Jean-Karim Hériché
- Toby Hodges
- Jelle Scholtalbers

As biological research becomes increasingly data-driven, the need is growing for careful, systematic management of this data. Methodical tracking, annotation, organisation, and storage of raw data, metadata, and data produced during and after analysis can now be considered essential to the success of projects.

In this new Bio-IT course, we will consider the challenges associated with effective data management in modern research, address the common pitfalls associated with this, and present a number of options implemented at EMBL to help make your projects more robust and efficient.

The course will focus on applied solutions and will mix taught elements with hands-on activities and exercises. Please bring a laptop along with you, or contact us if this is not possible for you.